/**
 * Copyright (c) 2011-2016, Jack Mo (mobangjack@foxmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#ifndef __USART3_H__
#define __USART3_H__

#include <stdint.h>

#define USART3_TX_FIFO_SIZE 256u

void USART3_Config(void);

void USART3_WriteByte(uint8_t byte);
void USART3_WriteBlock(uint8_t* pdata, uint8_t len);

#endif
